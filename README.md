# README
Find here the Scripts used to benchmark the Apache Parquet database of UN Comtrade data based on classification HS12, for all available observations, which was presented in our paper/presentation "[Providing large trade data sets for research using Apache Parquet and R Shiny](https://coms.events/NTTS2021/data/x_abstracts/x_abstract_165.pdf)" at the NTTS 2021.

If you have any questions regarding the paper or the code available, please contact us via [zenz@wiiw.ac.at](mailto:zenz@wiiw.ac.at) or [reiter@wiiw.ac.at](mailto:reiter@wiiw.ac.at)

## Scripts
- **NTTS_parquet.R** (R Script for benchmarking the Apache Parquet database)
- **NTTS_csv-rds-fst.R** (R Script for benchmarking csv, rds and the fst database)
- **NTTS_Stata.do** (Stata Script for benchmarking the database on the basis of Stata files)
- **99_test-statistics.R** (summary statistics and plots)

## Folder structure
The underlying folder structure of the databases is displayed below:

### Apache Parquet
```
Data/02_parquet
│── BEC
│   ├── 1988
│   ├── ...
│   └── 2019
│   │   ├── Export
│   │   │   ├── ABW
│   │   │   │   └── data.parquet
│   │   │   ├── ...
│   │   │   └── ZWE
│   │   │       └── data.parquet
│   │   ├── Import
│   │   ├── Re-Export
│   │   └── Re-Import
│── HS92
│── ...
│── HS12
│── STICrev1
│── ...
└── SITCrev4
```

### Benchmark formats (Stata, CSV, fst, rds)
Example _.rds_
```
Data/99_NTTS
│── flows
│   ├── rds
│   │   ├── Export
│   │   │   ├── ABW.rds
│   │   │   ├── AFG.rds
│   │   │   ├── AGO.rds
│   │   │   └── ...
│   │   ├── Import
│   │   │   ├── ABW.rds
│   │   │   ├── AFG.rds
│   │   │   ├── AGO.rds
│   │   │   └── ...
│   │   ├── Re-Import
│   │   │   └── ...
│   │   └── Re-Export
│   │       └── ...
│   ├── fst
│   │   └── ...
│   ├── rds-comp
│   │   └── ...
│   ├── fst-comp
│   │   └── ...
│   └── Stata
│       ├── Export
│       │   ├── ABW.dta
│       │   └── ...
│       └── ...
└── years
    ├── rds
    │   ├── 2012
    │   │   ├── ABW.rds
    │   │   └── ...
    │   ├── 2013
    │   │   └── ...
    │   └── ...
    │       └── ...
    ├── fst
    │   └── ...
    ├── rds-comp
    │   └── ...
    ├── fst-comp
    │   └── ...
    └── Stata
        ├── 2012
        │   ├── ABW.dta
        │   └── ...
        └── ...
            └── ...
```
