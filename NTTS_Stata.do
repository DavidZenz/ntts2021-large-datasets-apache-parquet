
/* /\* get the list of folders *\/ */
/* local folderList : dir "`dta_path'" dirs "*" */
/* /\* loop through folders *\/ */
/* foreach folder of local folderList { */
/*   /\* get list of files *\/ */
/*   local fileList : dir "`dta_path'/`folder'" files "filenameQ*" */
/*   /\* loop through files *\/ */
/*   foreach file of local fileList { */
/*     /\* do stuff to file *\/ */
/*     use `file' */
/*     /\* do more stuff *\/ */
/*   } */
/* } */

local dta_path "/home/zenz/R/Comtrade/Data/99_NTTS"
local eu28 "AUT BEL BGR HRV CYP CZE DNK EST FIN FRA DEU GRC HUN IRL ITA LVA LTU LUX MLT NLD POL PRT ROU SVK SVN ESP SWE"

timer clear


forvalues i = 1/5 {

di "Iteration: `i'"

/* -------------------------------------------------- */
di "Query #1"
clear
timer on 1
/* rep = AUT par = DEU year = 2017 product = TOTAL flow = Export */

use "`dta_path'/flows/Stata/Export/AUT.dta"
keep if PartnerISO == "DEU" & Year == 2017 & CommodityCode == "TOTAL"

/* /\* this version is faster *\/ */
/* use "`dta_path'/years/Stata/2017/AUT.dta" */
/* quietly keep if PartnerISO == "DEU" & CommodityCode == "TOTAL" & TradeFlow == "Export" */

timer off 1


/* -------------------------------------------------- */
di "Query #2"
clear
timer on 2
/* rep = AUT par = DEU year = 2012-2018 product = TOTAL flow = Export */

/* this version is faster */
use "`dta_path'/flows/Stata/Export/AUT.dta"
quietly keep if PartnerISO == "DEU" & Year >= 2012 & Year <= 2019 & CommodityCode == "TOTAL"

/* forvalues year = 2012/2019 { */
/*           /\* di "`dta_path'/years/Stata/`year'/AUT.dta" *\/ */
/*           append using "`dta_path'/years/Stata/`year'/AUT.dta" */
/* } */
/* quietly keep if PartnerISO == "DEU" & CommodityCode == "TOTAL" & TradeFlow == "Export" */

timer off 2


/* -------------------------------------------------- */
di "Query #3"
clear
timer on 3
/* rep = EU28 par = USA year = 2017 product = TOTAL flow = Export */

/* 92 seconds */
foreach rep of local eu28 {
          di "`dta_path'/flows/Stata/Export/`rep'.dta"
          append using "`dta_path'/flows/Stata/Export/`rep'.dta"
}
quietly keep if PartnerISO == "USA" & CommodityCode == "TOTAL" & Year == 2017

/* /\* 11 seconds, this version is faster *\/ */
/* foreach rep of local eu28 { */
/*           /\* di "`dta_path'/years/Stata/2017/`rep'.dta" *\/ */
/*           append using "`dta_path'/years/Stata/2017/`rep'.dta" */
/*           quietly keep if PartnerISO == "USA" & CommodityCode == "TOTAL" & TradeFlow == "Export" */
/* } */

timer off 3


/* -------------------------------------------------- */
di "Query #4"
clear
timer on 4
/* rep = EU28 par = USA/JPN/KOR/RUS/BRA year = 2017 product = TOTAL flow = Export/Import */

/* 94 seconds */
foreach rep of local eu28 {
          di "`dta_path'/flows/Stata/Export/`rep'.dta"
          append using "`dta_path'/flows/Stata/Export/`rep'.dta"
          append using "`dta_path'/flows/Stata/Import/`rep'.dta"
          quietly keep if inlist(PartnerISO, "USA", "JPN", "KOR", "RUS", "BRA") & CommodityCode == "TOTAL" & Year == 2017
}

/* /\* 12 seconds, this version is faster *\/ */
/* foreach rep of local eu28 { */
/*           /\* di "`dta_path'/years/Stata/2017/`rep'.dta" *\/ */
/*           append using "`dta_path'/years/Stata/2017/`rep'.dta" */
/*           quietly keep if inlist(PartnerISO, "USA", "JPN", "KOR", "RUS", "BRA") & CommodityCode == "TOTAL" & inlist(TradeFlow, "Export", "Import") */
/* } */

timer off 4


/* -------------------------------------------------- */
di "Query #5"
clear
timer on 5
/* rep = EU28 par = USA/JPN/KOR/RUS/BRA year = 2012-2019 product = TOTAL flow = Export/Import */

/* equally slow */
foreach rep of local eu28 {
          /* di "`dta_path'/flows/Stata/Export/`rep'.dta" */
          append using "`dta_path'/flows/Stata/Export/`rep'.dta"
          append using "`dta_path'/flows/Stata/Import/`rep'.dta"
          quietly keep if inlist(PartnerISO, "USA", "JPN", "KOR", "RUS", "BRA") & CommodityCode == "TOTAL" & inlist(Year, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019)
}

/* foreach rep of local eu28 { */
/* forvalues year = 2012/2019 { */
/* di "`dta_path'/years/Stata/`year'/`rep'.dta" */
/* append using "`dta_path'/years/Stata/`year'/`rep'.dta" */
/* } */
/* quietly keep if inlist(PartnerISO, "USA", "JPN", "KOR", "RUS", "BRA") & CommodityCode == "TOTAL" & inlist(TradeFlow, "Export", "Import") */
/* } */

timer off 5

}

timer list

/* /\* Using best classification *\/ */
/* . timer list */
/*    1:      1.58 /        5 =       0.3170 */
/*    2:      7.07 /        5 =       1.4146 */
/*    3:     55.53 /        5 =      11.1070 */
/*    4:     58.65 /        5 =      11.7308 */
/*    5:    480.96 /        5 =      96.1914 */


/* /\* Using years-classification *\/ */
/* . timer list */
/*    1:      1.40 /        5 =       0.2790 */
/*    2:     22.21 /        5 =       4.4416 */
/*    3:     55.81 /        5 =      11.1614 */
/*    4:     60.84 /        5 =      12.1680 */
/*    5:    510.66 /        5 =     102.1328 */


/* /\* Using flows-classification *\/ */
/* . timer list */
/*    1:      5.32 /        5 =       1.0642 */
/*    2:      4.52 /        5 =       0.9050 */
/*    3:    454.95 /        5 =      90.9910 */
/*    4:    496.12 /        5 =      99.2244 */
/*    5:    499.27 /        5 =      99.8532 */
